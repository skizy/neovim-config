local transparent = true

if vim.g.neovide then
	transparent = false
end

return {
	{ -- rose-pine/neovim
		"rose-pine/neovim",
		name = "rose-pine",
		lazy = false,
		priority = 1000,
		opts = {
			variant = "moon",
			dark_variant = "moon",
			styles = {
				transparency = transparent,
				italic = false,
			},
			highlight_groups = {
				Folded = { bg = "surface" },
				NormalFloat = { bg = "surface" },
				CursorLine = { bg = "overlay" },
				Pmenu = { bg = "surface" },
				Comment = { fg = "muted" },

				TelescopeTitle = { fg = "rose" },
				TelescopeBorder = { fg = "foam", bg = "none" },
				TelescopeNormal = { bg = "none" },
				TelescopePromptNormal = { bg = "none" },
				TelescopeResultsNormal = { fg = "subtle", bg = "none" },
				TelescopeSelection = { fg = "text", bg = "none" },
				TelescopeSelectionCaret = { fg = "rose", bg = "none" },

				FloatBorder = { fg = "foam", bg = "none" },

				IblIndent = { fg = "#2a273f" },
				IblScope = { fg = "foam" },

				-- (2) Underline instead of undercurl for diagnostics
				-- DiagnosticUnderlineError = { sp = groups.error, underline = true },
				-- DiagnosticUnderlineHint = { sp = groups.hint, underline = true },
				-- DiagnosticUnderlineInfo = { sp = groups.info, underline = true },
				-- DiagnosticUnderlineWarn = { sp = groups.warn, underline = true },

				-- This shit doesn't work if you set bg to none, but works this way, idk why
				GitSignsDeleteNr = { fg = "rose", bg = "none" },
				GitSignsDelete = { fg = "rose", bg = "none" },
				GitSignsChangeNr = { fg = "pine", bg = "none" },
				GitSignsChange = { fg = "pine", bg = "none" },
				GitSignsAddNr = { fg = "foam", bg = "none" },
				GitSignsAdd = { fg = "foam", bg = "none" },

				-- MsgArea = { fg = "text", bg = "base" },
			},
		},
	},
	{ -- nyoom-engineering/oxocarbon.nvim
		"nyoom-engineering/oxocarbon.nvim",
		-- lazy = false,
	},
	{ -- wuelnerdotexe/vim-enfocado
		"wuelnerdotexe/vim-enfocado",
		-- lazy = false,
	},
	{ -- maxmx03/FluoroMachine.nvim
		"maxmx03/FluoroMachine.nvim",
		-- lazy = false,
		opts = {
			glow = true,
			brightness = 0.2,
			transparent = "full",
		},
	},
	{ -- zootedb0t/citruszest.nvim
		"zootedb0t/citruszest.nvim",
		-- lazy = false,
		priority = 1000,
		opts = {
			option = {
				transparent = true,
			},
		},
	},
	{ -- folke/tokyonight.nvim
		"folke/tokyonight.nvim",
		lazy = false,
		priority = 1000,
		opts = {
			transparent = true,
			on_highlights = function(hl, colors)
				hl.Folded.bg = colors.bg_dark
				hl.LineNr.fg = colors.dark3
				hl.CursorLineNr.fg = colors.blue
			end,
		},
	},
}
