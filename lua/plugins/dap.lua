return {
	{ -- mfussenegger/nvim-dap
		"mfussenegger/nvim-dap",
		ft = { "rust", "python" },
		config = function()
			local dap = require("dap")
			local dapui = require("dapui")

			dap.listeners.after.event_initialized["dapui_config"] = dapui.open
			dap.listeners.before.event_terminated["dapui_config"] = dapui.close
			dap.listeners.before.event_exited["dapui_config"] = dapui.close

			vim.keymap.set("n", "<F2>", dap.toggle_breakpoint, { desc = "DAP: toggle breakpoint" })
			vim.keymap.set("n", "<F3>", dap.continue, { desc = "DAP: continue" })
			vim.keymap.set("n", "<F4>", dap.step_over, { desc = "DAP: step over" })
			vim.keymap.set("n", "<F5>", dap.step_into, { desc = "DAP: step into" })
			-- running debugger keymap is set locally, see 'lsp.lua' rust section for example (or the debugger is run with 'continue')
		end
	},
	{ -- rcarriga/nvim-dap-ui
		"rcarriga/nvim-dap-ui",
		dependencies = { "nvim-dap" },
		opts = {
			icons = { expanded = '▾', collapsed = '▸', current_frame = '*' },
			controls = {
				icons = {
					pause = '⏸',
					play = '▶',
					step_into = '⏎',
					step_over = '⏭',
					step_out = '⏮',
					step_back = 'b',
					run_last = '▶▶',
					terminate = '⏹',
					disconnect = "⏏",
				},
			},
		},
	},
	{ -- mfussenegger/nvim-dap-python
		"mfussenegger/nvim-dap-python",
		config = function()
			require("dap-python").setup("/home/skizy/stuff/virtualenvs/debugpy/bin/python")
		end
	},
}
