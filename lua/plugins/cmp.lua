return {
	{ -- hrsh7th/cmp-nvim-lsp
		"hrsh7th/cmp-nvim-lsp",
	},
	{ -- hrsh7th/cmp-vsnip
		"hrsh7th/cmp-vsnip",
	},
	{ -- hrsh7th/cmp-path
		"hrsh7th/cmp-path",
	},
	{ -- hrsh7th/cmp-buffer
		"hrsh7th/cmp-buffer",
	},
	{ -- hrsh7th/vim-vsnip
		"hrsh7th/vim-vsnip",
		init = function ()
			vim.g.vsnip_snippet_dir = "/home/skizy/.config/nvim/snippets"
		end
	},
	{ -- hrsh7th/nvim-cmp
		"hrsh7th/nvim-cmp",
		event = "VeryLazy",
		config = function()
			require("lazy").load({
				plugins = {
					"cmp-nvim-lsp",
					"cmp-vsnip",
					"cmp-path",
					"cmp-buffer",
					"vim-vsnip",
				},
			})

			local has_words_before = function()
				unpack = unpack or table.unpack
				local line, col = unpack(vim.api.nvim_win_get_cursor(0))
				return col ~= 0 and
					vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
			end

			local feedkey = function(key, mode)
				vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(key, true, true, true), mode, true)
			end

			local cmp = require("cmp")
			cmp.setup({
				-- Enable LSP snippets
				snippet = {
					expand = function(args)
						vim.fn["vsnip#anonymous"](args.body)
					end,
				},
				mapping = {
					["<C-p>"] = cmp.mapping.select_prev_item(),
					["<C-n>"] = cmp.mapping.select_next_item(),

					-- Add tab support
					-- ["<S-Tab>"] = cmp.mapping.select_prev_item(),
					-- ["<Tab>"] = cmp.mapping.select_next_item(),
					["<Tab>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							cmp.select_next_item()
						elseif vim.fn["vsnip#available"](1) == 1 then
							feedkey("<Plug>(vsnip-expand-or-jump)", "")
						elseif has_words_before() then
							cmp.complete()
						else
							fallback()
						end
					end, { "i", "s" }),
					["<S-Tab>"] = cmp.mapping(function()
						if cmp.visible() then
							cmp.select_prev_item()
						elseif vim.fn["vsnip#jumpable"](-1) == 1 then
							feedkey("<Plug>(vsnip-jump-prev)", "")
						end
					end, { "i", "s" }),

					["<C-d>"] = cmp.mapping.scroll_docs(-4),
					["<C-f>"] = cmp.mapping.scroll_docs(4),
					["<C-Space>"] = cmp.mapping.complete(),
					["<C-e>"] = cmp.mapping.close(),
					["<CR>"] = cmp.mapping.confirm({
						behavior = cmp.ConfirmBehavior.Insert,
						select = false,
					})
				},

				-- Installed sources
				sources = {
					{ name = "nvim_lsp" },
					{ name = "vsnip" },
					{ name = "path" },
					{ name = "buffer" },
					{ name = "crates" },
				},

				preselect = cmp.PreselectMode.None

			})
		end,
	},
}
