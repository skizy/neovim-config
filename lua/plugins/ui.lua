return {
	{ -- rcarriga/nvim-notify
		"rcarriga/nvim-notify",
		enabled = false,
		opts = {
			background_colour = "#232136",
		},
		version = "*",
		event = "VeryLazy",
	},
	{ -- folke/noice.nvim
		"folke/noice.nvim",
		enabled = false,
		dependencies = {
			{ "MunifTanjim/nui.nvim", version = "*" },
			"rcarriga/nvim-notify",
		},
		priority = 200,
		version = "*",
		opts = {
			lsp = {
				-- override markdown rendering so that **cmp** and other plugins use **Treesitter**
				override = {
					["vim.lsp.util.convert_input_to_markdown_lines"] = true,
					["vim.lsp.util.stylize_markdown"] = true,
					["cmp.entry.get_documentation"] = true,
				},
				-- signature = {
				-- 	auto_open = {
				-- 		enabled = false,
				-- 	},
				-- },
			},
			presets = {
				command_palette = true, -- position the cmdline and popupmenu together
				long_message_to_split = true, -- long messages will be sent to a split
				inc_rename = true, -- enables an input dialog for inc-rename.nvim
				lsp_doc_border = true, -- add a border to hover docs and signature help
			},
		},
		event = "VeryLazy",
	},
	{ -- stevearc/dressing.nvim
		"stevearc/dressing.nvim",
		opts = {
			select = {
				telescope = require("telescope.themes").get_cursor(),
			},
		},
		event = "VeryLazy",
	},
	{ -- nvim-lualine/lualine.nvim
		"nvim-lualine/lualine.nvim",
		config = function()
			-- local noice = require("noice")
			require("lualine").setup({
				options = {
					refresh = {
						statusline = 500,
					},
					-- component_separators = { left = "", right = "" },
					-- section_separators = { left = "", right = "" },
					component_separators = { left = "│", right = "│" },
					section_separators = { left = "", right = "" },
				},
				-- sections = {
				-- 	lualine_x = {
				-- 		{
				-- 			noice.api.status.mode.get,
				-- 			cond = noice.api.status.mode.has,
				-- 		},
				-- 		{
				-- 			noice.api.status.search.get,
				-- 			cond = noice.api.status.search.has,
				-- 		},
				-- 		"encoding",
				-- 		"fileformat",
				-- 		"filetype",
				-- 	},
				-- },
			})

			vim.api.nvim_create_autocmd("BufWritePost", {
				pattern = "?*",
				callback = require("lualine").refresh,
			})

			vim.api.nvim_create_autocmd("RecordingEnter", {
				callback = require("lualine").refresh,
			})
			vim.api.nvim_create_autocmd("RecordingLeave", {
				callback = function()
					vim.schedule(require("lualine").refresh)
				end,
			})
		end,
		event = "VeryLazy",
	},
	{ -- akinsho/bufferline.nvim
		"akinsho/bufferline.nvim",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		enabled = false,
		opts = {
			options = {
				separator_style = "slope",
				show_buffer_close_icons = false,
				show_close_icon = false,
				diagnostics = "nvim_lsp",
				diagnostics_indicator = function(_, _, diagnostics_dict, _)
					local s = " "
					for e, n in pairs(diagnostics_dict) do
						local sym = e == "error" and " " or (e == "warning" and " " or " ")
						s = s .. sym .. n .. " "
					end
					return string.sub(s, 1, -2)
				end,
			},
			highlights = {
				fill = {
					bg = "#191723",
				},

				background = {
					bg = "#232136",
				},
				buffer_selected = {
					bg = "#393552",
				},
				buffer_visible = {
					bg = "#2a273f",
				},

				duplicate = {
					bg = "#232136",
				},
				duplicate_selected = {
					bg = "#393552",
				},
				duplicate_visible = {
					bg = "#2a273f",
				},

				separator = {
					fg = "#191724",
					bg = "#232136",
				},
				separator_selected = {
					fg = "#191724",
					bg = "#393552",
				},
				separator_visible = {
					fg = "#191724",
					bg = "#2a273f",
				},

				tab = {
					bg = "#232136",
				},
				tab_selected = {
					fg = "#e0def4",
					bg = "#393552",
				},

				tab_separator = {
					fg = "#191724",
					bg = "#232136",
				},
				tab_separator_selected = {
					fg = "#191724",
					bg = "#393552",
				},

				modified = {
					bg = "#232136",
				},
				modified_selected = {
					bg = "#393552",
				},
				modified_visible = {
					bg = "#2a273f",
				},

				diagnostic = {
					bg = "#232136",
				},
				diagnostic_visible = {
					bg = "#2a273f",
				},
				diagnostic_selected = {
					bg = "#393552",
				},

				hint = {
					-- fg = '<colour-value-here>',
					-- sp = '<colour-value-here>',
					bg = "#232136",
				},
				hint_visible = {
					-- fg = '<colour-value-here>',
					bg = "#2a273f",
				},
				hint_selected = {
					-- fg = '<colour-value-here>',
					bg = "#393552",
					-- sp = '<colour-value-here>',
					-- bold = true,
					-- italic = true,
				},
				hint_diagnostic = {
					-- fg = '<colour-value-here>',
					-- sp = '<colour-value-here>',
					bg = "#232136",
				},
				hint_diagnostic_visible = {
					-- fg = '<colour-value-here>',
					bg = "#2a273f",
				},
				hint_diagnostic_selected = {
					-- fg = '<colour-value-here>',
					bg = "#393552",
					-- sp = '<colour-value-here>',
					-- bold = true,
					-- italic = true,
				},
				info = {
					-- fg = '<colour-value-here>',
					-- sp = '<colour-value-here>',
					bg = "#232136",
				},
				info_visible = {
					-- fg = '<colour-value-here>',
					bg = "#2a273f",
				},
				info_selected = {
					-- fg = '<colour-value-here>',
					bg = "#393552",
					-- sp = '<colour-value-here>',
					-- bold = true,
					-- italic = true,
				},
				info_diagnostic = {
					-- fg = '<colour-value-here>',
					-- sp = '<colour-value-here>',
					bg = "#232136",
				},
				info_diagnostic_visible = {
					-- fg = '<colour-value-here>',
					bg = "#2a273f",
				},
				info_diagnostic_selected = {
					-- fg = '<colour-value-here>',
					bg = "#393552",
					-- sp = '<colour-value-here>',
					-- bold = true,
					-- italic = true,
				},
				warning = {
					-- fg = '<colour-value-here>',
					-- sp = '<colour-value-here>',
					bg = "#232136",
				},
				warning_visible = {
					-- fg = '<colour-value-here>',
					bg = "#2a273f",
				},
				warning_selected = {
					-- fg = '<colour-value-here>',
					bg = "#393552",
					-- sp = '<colour-value-here>',
					-- bold = true,
					-- italic = true,
				},
				warning_diagnostic = {
					-- fg = '<colour-value-here>',
					-- sp = '<colour-value-here>',
					bg = "#232136",
				},
				warning_diagnostic_visible = {
					-- fg = '<colour-value-here>',
					bg = "#2a273f",
				},
				warning_diagnostic_selected = {
					-- fg = '<colour-value-here>',
					bg = "#393552",
					-- sp = '<colour-value-here>',
					-- bold = true,
					-- italic = true,
				},
				error = {
					-- fg = '<colour-value-here>',
					bg = "#232136",
					-- sp = '<colour-value-here>',
				},
				error_visible = {
					-- fg = '<colour-value-here>',
					bg = "#2a273f",
				},
				error_selected = {
					-- fg = '<colour-value-here>',
					bg = "#393552",
					-- sp = '<colour-value-here>',
					-- bold = true,
					-- italic = true,
				},
				error_diagnostic = {
					-- fg = '<colour-value-here>',
					bg = "#232136",
					-- sp = '<colour-value-here>',
				},
				error_diagnostic_visible = {
					-- fg = '<colour-value-here>',
					bg = "#2a273f",
				},
				error_diagnostic_selected = {
					-- fg = '<colour-value-here>',
					bg = "#393552",
					-- sp = '<colour-value-here>',
					-- bold = true,
					-- italic = true,
				},
			},
		},
		config = true,
		event = "VeryLazy",
	},
	{ -- j-hui/fidget.nvim
		"j-hui/fidget.nvim",
		opts = {},
		event = "VeryLazy",
	},
}
