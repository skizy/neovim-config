return {
	{ -- neovim/nvim-lspconfig
		"neovim/nvim-lspconfig",
		config = function()
			require("lspconfig.ui.windows").default_options = {
				border = "rounded",
			}

			local lspconfig = require("lspconfig")

			require("neodev") -- load neodev before lua_ls setup

			lspconfig.pyright.setup({}) -- must supply the argument
			-- TODO: change linter from pyright to ruff, but leave pyright for autocompletion
			local configs = require("lspconfig.configs")
			if not configs.ruff_lsp then
				configs.ruff_lsp = {
					default_config = {
						cmd = { "ruff-lsp" },
						filetypes = { "python" },
						root_dir = require("lspconfig").util.find_git_ancestor,
						init_options = {
							settings = {
								args = {},
							},
						},
					},
				}
			end
			lspconfig.ruff_lsp.setup({})
			lspconfig.lua_ls.setup({
				-- on_attach = require("lsp-format").on_attach,
				settings = {
					Lua = {
						runtime = {
							version = "LuaJIT",
						},
						telemetry = {
							enable = false,
						},
					},
				},
			})

			local capabilities = vim.lsp.protocol.make_client_capabilities()
			capabilities.textDocument.completion.completionItem.snippetSupport = true

			lspconfig.html.setup({ capabilities = capabilities })
			lspconfig.cssls.setup({ capabilities = capabilities })
			-- lspconfig.eslint.setup({ capabilities = capabilities })
			lspconfig.biome.setup({})

			-- local util = require("lspconfig.util")

			lspconfig.svelte.setup({
				-- filetypes = { "svelte", "typescript", "javascript" },
				-- root_dir = function (fname)
				-- 	if vim.bo.filetype == "svelte" then
				-- 		return util.root_pattern("package.json", ".git")(fname)
				-- 		-- svelte
				-- 	elseif vim.bo.filetype == "typescript" or vim.bo.filetype == "javascript" then
				-- 		return util.root_pattern(".nvim_svelte")(fname)
				-- 		-- js
				-- 	end
				-- end
			})

			require("inc_rename") -- load inc-rename.nvim

			vim.keymap.set("n", "<Leader>vd", vim.diagnostic.open_float, { desc = "(V)iew (d)iagnostic float" })
			vim.keymap.set("n", "<Leader>vh", vim.lsp.buf.hover, { desc = "(V)iew (h)over" })
			vim.keymap.set("n", "<Leader>a", vim.lsp.buf.code_action, { desc = "Code (a)ctions" })
			vim.keymap.set("n", "<Leader>rn", vim.lsp.buf.rename, { desc = "(R)e(n)ame under cursor" })
			vim.keymap.set("n", "gd", "<cmd>TroubleToggle lsp_definitions<cr>", { desc = "(G)o to (d)efinition" })
			vim.keymap.set(
				"n",
				"<Leader>gt",
				"<cmd>TroubleToggle lsp_type_definitions<cr>",
				{ desc = "(G)o to (t)ype definition" }
			)
			vim.keymap.set(
				"n",
				"<Leader>td",
				"<cmd>TroubleToggle workspace_diagnostics<cr>",
				{ desc = "(T)oggle workspace (d)iagnostics" }
			)
			vim.keymap.set(
				"n",
				"<Leader>dd",
				"<cmd>TroubleToggle document_diagnostics<cr>",
				{ desc = "Toggle (d)ocument (d)iagnostics" }
			)
			vim.keymap.set(
				"n",
				"<Leader>rf",
				"<cmd>TroubleToggle lsp_references<cr>",
				{ desc = "Toggle trouble (r)e(f)erences" }
			)
		end,
		ft = {
			"lua",
			"python",
			"html",
			"css",
			"scss",
			"less",
			"javascript",
			"javascriptreact",
			"typescript",
			"typescriptreact",
			"svelte",
		},
	},
	{ -- folke/neodev.nvim
		"folke/neodev.nvim",
		opts = {
			library = { plugins = { "nvim-dap-ui" }, types = true },
		},
		config = true,
	},
	-- NOTE: Disabled it, because it messes up folding with lua_ls
	-- To enable, set on_attach in laguage server settings
	{ "lukas-reineke/lsp-format.nvim", enabled = false, config = true },
	{ -- stevearc/conform.nvim
		"stevearc/conform.nvim",
		keys = {
			{
				"<Leader>fm",
				function()
					require("conform").format({ lsp_fallback = true })
				end,
				desc = "(F)or(m)at buffer with LSP",
			},
		},
		opts = {
			formatters_by_ft = {
				javascript = { "biome" },
				javascriptreact = { "biome" },
				typescript = { "biome" },
				typescriptreact = { "biome" },
				svelte = { "prettier" },

				rust = { "rustfmt" },
				dart = { "dart_format" },
				lua = { "stylua" },
				python = { "ruff_format", "black" },
			},
			formatters = {
				rustfmt = {
					prepend_args = { "--edition", "2021" },
				},
			},
		},
	},
	{ -- mhartington/formatter.nvim
		"mhartington/formatter.nvim",
		enabled = false,
		ft = { "javascript", "javascriptreact", "typescript", "typescriptreact" },
		config = function()
			local util = require("formatter.util")

			local prettier_config = function()
				return {
					exe = "prettier",
					args = {
						"--tab-width",
						"4",
						"--stdin-filepath",
						util.escape_path(util.get_current_buffer_file_path()),
					},
					stdin = true,
					try_node_modules = true,
				}
			end

			require("formatter").setup({
				filetype = {
					javascript = { prettier_config },
					javascriptreact = { prettier_config },
					typescript = { prettier_config },
					typescriptreact = { prettier_config },
				},
			})

			vim.keymap.set("n", "<Leader>fm", "<cmd>Format<cr>", { desc = "(F)or(m)at buffer with formatter.nvim" })
		end,
	},
	{ -- simrat39/rust-tools.nvim
		"simrat39/rust-tools.nvim",
		enabled = false,
		dependencies = { "nvim-lspconfig" },
		config = function()
			local extension_path = "/home/skizy/.local/share/codelldb/extension/"
			local codelldb_path = extension_path .. "adapter/codelldb"
			local liblldb_path = extension_path .. "lldb/lib/liblldb.so"

			require("rust-tools").setup({
				tools = {
					-- rust-tools options
					autoSetHints = true,
					-- hover_with_actions = true,
					inlay_hints = {
						show_parameter_hints = false,
						parameter_hints_prefix = "",
						other_hints_prefix = "",
					},
				},

				-- all the opts to send to nvim-lspconfig
				-- these override the defaults set by rust-tools.nvim
				-- see https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#rust_analyzer
				server = {
					-- on_attach is a callback called when the language server attachs to the buffer
					on_attach = function(_)
						-- require("lsp-format").on_attach(client)

						vim.keymap.set(
							"n",
							"<F6>",
							"<cmd>RustDebuggables<cr>",
							{ buffer = true, desc = "Run debugger" }
						)
					end,
					settings = {
						-- to enable rust-analyzer settings visit:
						-- https://github.com/rust-analyzer/rust-analyzer/blob/master/docs/user/generated_config.adoc
						["rust-analyzer"] = {
							-- enable clippy on save
							check = {
								command = "clippy",
							},
							signatureInfo = {
								-- detail = "parameters", -- by default shows "full" signature
								documentation = {
									enable = false,
								},
							},
							diagnostics = {
								-- for esp rust fork
								disabled = { "unresolved-proc-macro" },
							},
						},
					},
				},
				dap = {
					adapter = require("rust-tools.dap").get_codelldb_adapter(codelldb_path, liblldb_path),
				},
			})
		end,
		ft = "rust",
	},
	{ -- mrcjkb/rustaceanvim
		"mrcjkb/rustaceanvim",
		version = "^4", -- Recommended
		config = function()
			vim.g.rustaceanvim = {
				server = {
					on_attach = function(client, bufnr)
						require("lsp-inlayhints").on_attach(client, bufnr)
					end,
				},
			}
		end,
		ft = { "rust" },
	},
	{ -- lvimuser/lsp-inlayhints.nvim
		"lvimuser/lsp-inlayhints.nvim",
		opts = {
			inlay_hints = {
				highlight = "Comment",
			},
		},
		ft = { "rust" },
	},
	{ -- akinsho/flutter-tools.nvim
		"akinsho/flutter-tools.nvim",
		dependencies = { "nvim-lua/plenary.nvim", "cmp-nvim-lsp" },
		keys = {
			{
				"<Leader>fls",
				"<cmd>FlutterRun<cr>",
			},
			{
				"<Leader>flr",
				"<cmd>FlutterRestart<cr>",
			},
			{
				"<Leader>flh",
				"<cmd>FlutterReload<cr>",
			},
			{
				"<Leader>fla",
				"<cmd>FlutterReanalyze<cr>",
			},
			{
				"<Leader>flq",
				"<cmd>FlutterQuit<cr>",
			},
		},
		init = function()
			vim.g.dart_style_guide = 2
		end,
		config = function()
			require("flutter-tools").setup({
				lsp = {
					capabilities = require("cmp_nvim_lsp").default_capabilities(
						vim.lsp.protocol.make_client_capabilities()
					),
					-- on_attach = require("lsp-format").on_attach,
				},
			})
			require("lspconfig")
		end,
		ft = "dart",
	},
	{ -- simrat39/symbols-outline.nvim
		"simrat39/symbols-outline.nvim",
		opts = {
			auto_close = true,
			show_relative_numbers = true,
			autofold_depth = 1,
			width = 40,
		},
		keys = {
			{
				"<F7>",
				"<cmd>SymbolsOutline<cr>",
				desc = "Toggle buffer symbols",
			},
		},
		ft = {
			"dart",
			"rust",
			"lua",
			"python",
			"typescript",
			"typescriptreact",
			"javascript",
			"javascriptreact",
			"svelte",
		},
		config = function(_, opts)
			require("symbols-outline").setup(opts)
		end,
	},
	{ -- pmizio/typescript-tools.nvim
		"pmizio/typescript-tools.nvim",
		dependencies = { "nvim-lua/plenary.nvim", "neovim/nvim-lspconfig" },
		config = function()
			require("typescript-tools").setup({
				on_attach = function(client, bufnr)
					require("lsp-inlayhints").on_attach(client, bufnr)
				end,
				settings = {
					tsserver_file_preferences = {
						includeInlayParameterNameHints = "all",
						includeInlayEnumMemberValueHints = true,
						includeInlayFunctionLikeReturnTypeHints = true,
						includeInlayFunctionParameterTypeHints = true,
						includeInlayPropertyDeclarationTypeHints = true,
						includeInlayVariableTypeHints = true,
					},
				},
			})
		end,
		ft = {
			"html",
			"css",
			"scss",
			"less",
			"javascript",
			"javascriptreact",
			"typescript",
			"typescriptreact",
			"svelte",
		},
	},
}
