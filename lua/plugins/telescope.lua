return {
	{ "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
	{ -- nvim-telescope/telescope.nvim
		"nvim-telescope/telescope.nvim",
		dependencies = { "plenary.nvim" },
		event = "VeryLazy",
		opts = {
			defaults = {
				vimgrep_arguments = {
					"rg",
					"--color=never",
					"--no-heading",
					"--with-filename",
					"--line-number",
					"--column",
					"--smart-case",
					"--trim"
				},
				file_ignore_patterns = {
					".git",
				},
				-- mappings = {
				-- 	i = { ["<c-t>"] = trouble.open_with_trouble },
				-- 	n = { ["<c-t>"] = trouble.open_with_trouble },
				-- },
			},
			extensions = {
				fzf = {
					fuzzy = true,    -- false will only do exact matching
					override_generic_sorter = true, -- override the generic sorter
					override_file_sorter = true, -- override the file sorter
					case_mode = "smart_case", -- or "ignore_case" or "respect_case"
				},
			},
		},
		---@param _ any
		---@param opts table
		config = function(_, opts)
			local trouble = require("trouble.providers.telescope")
			opts.defaults.mappings = {
				i = { ["<c-t>"] = trouble.open_with_trouble },
				n = { ["<c-t>"] = trouble.open_with_trouble },
			}
			require("telescope").setup(opts)

			require("telescope").load_extension("fzf")

			vim.keymap.set("n", "<Leader>ff", function() require('telescope.builtin').find_files({ hidden = true }) end,
				{ desc = "Telescope: (f)ind (f)iles" })
			vim.keymap.set("n", "<Leader>fh",
				function() require('telescope.builtin').find_files({ cwd = "/home/skizy/", hidden = true }) end,
				{ desc = "Telescope: (f)ind (h)idden files in home" })
			vim.keymap.set("n", "<Leader>fd", function() require('telescope.builtin').find_files({ cwd = "/home/skizy/" }) end,
				{ desc = "Telescope: (f)in(d) files in home" })
			vim.keymap.set("n", "<Leader>fc",
				function() require('telescope.builtin').find_files({ cwd = "/home/skizy/.config/", hidden = true }) end,
				{ desc = "Telescope: (f)ind (c)onfig files" })
			vim.keymap.set("n", "<Leader>lg", require('telescope.builtin').live_grep, { desc = "Telescope: (l)ive (g)rep" })
			vim.keymap.set("n", "<Leader>lb",
				function() require('telescope.builtin').live_grep({ search_dirs = { vim.api.nvim_buf_get_name(0) } }) end,
				{ desc = "Telescope: (l)ive grep current (b)uffer" })
			vim.keymap.set("n", "<Leader>b", require("telescope.builtin").buffers, { desc = "Telescope: (b)uffers" })
			vim.keymap.set("n", "<Leader>sp", function()
				require("telescope.builtin").spell_suggest(require("telescope.themes").get_cursor({
					layout_config = { width = 40, height = 15, },
				}))
			end, { desc = "Telescope: (s)(p)ell suggest" })
		end
	},
	{ -- debugloop/telescope-undo.nvim
		"debugloop/telescope-undo.nvim",
		dependencies = {
			{
				"nvim-telescope/telescope.nvim",
				dependencies = { "plenary.nvim" },
			},
		},
		keys = {
			{
				"<Leader>u",
				"<cmd>Telescope undo<cr>",
				desc = "Telescope Undo",
			},
		},
		opts = {
			extensions = {
				undo = {},
			},
		},
		config = function(_, opts)
			require("telescope").setup(opts)
			require("telescope").load_extension("undo")
		end,
	},
}
