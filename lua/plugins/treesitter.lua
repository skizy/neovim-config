return {
	{ -- nvim-treesitter
		"nvim-treesitter/nvim-treesitter",
		opts = {
			highlight = {
				enable = true,
				disable = { "rust", "lua" },
			},
			indent = {
				enable = { "html", "javascript", "typescript" },
			},
			playground = {
				enable = true,
				disable = {},
				updatetime = 25, -- Debounced time for highlighting nodes in the playground from source code
				persist_queries = false, -- Whether the query persists across vim sessions
				keybindings = {
					toggle_query_editor = "o",
					toggle_hl_groups = "i",
					toggle_injected_languages = "t",
					toggle_anonymous_nodes = "a",
					toggle_language_display = "I",
					focus_language = "f",
					unfocus_language = "F",
					update = "R",
					goto_node = "<cr>",
					show_help = "?",
				},
			},
			ensure_installed = { "rust", "lua", "python", "query", "markdown", "rasi" },
		},
		config = function(_, opts)
			require("nvim-treesitter.configs").setup(opts)
			-- 			vim.treesitter.query.set("rust", "folds",
			-- 				[[
			-- 	[
			-- 		(mod_item)
			-- 		(function_item)
			-- 		(struct_item)
			-- 		(trait_item)
			-- 		(enum_item)
			-- 		(impl_item)
			-- 		(use_declaration)
			-- 		(match_expression)
			-- 		(array_expression)
			-- 	] @fold
			-- ]])
		end,
		event = "VeryLazy",
		lazy = false,
	},
	{ -- nvim-treesitter/playground
		"nvim-treesitter/playground",
		lazy = false,
	},
	{ -- nvim-treesitter-context
		"nvim-treesitter/nvim-treesitter-context",
		event = "VeryLazy",
		opts = {
			enable = true, -- Enable this plugin (Can be enabled/disabled later via commands)
			max_lines = 0, -- How many lines the window should span. Values <= 0 mean no limit.
			min_window_height = 0, -- Minimum editor window height to enable context. Values <= 0 mean no limit.
			line_numbers = true,
			multiline_threshold = 20, -- Maximum number of lines to collapse for a single context line
			trim_scope = "outer", -- Which context lines to discard if `max_lines` is exceeded. Choices: "inner", "outer"
			mode = "cursor", -- Line used to calculate context. Choices: "cursor", "topline"
			-- Separator between context and content. Should be a single character string, like "-".
			-- When separator is set, the context will only show up when there are at least 2 lines above cursorline.
			separator = nil,
			zindex = 20, -- The Z-index of the context window
		},
		config = function(_, opts)
			require("treesitter-context").setup(opts)
		end,
	},
	{ -- luckasRanarison/tree-sitter-hyprlang
		"luckasRanarison/tree-sitter-hyprlang",
		ft = "hyprlang",
	},
}
