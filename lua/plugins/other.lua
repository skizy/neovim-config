return {
	{ -- saecki/crates.nvim
		"saecki/crates.nvim",
		dependencies = { "plenary.nvim" },
		version = "v0.3.0",
		ft = "toml",
		config = function()
			local crates = require("crates")
			crates.setup()
			crates.reload()

			-- print(vim.inspect(require("config.keymap")))
			vim.keymap.set("n", "<Leader>cr", crates.reload, { desc = "(C)rates: (r)eload" })

			vim.keymap.set("n", "<Leader>cv", crates.show_versions_popup, { desc = "(C)rates: show (v)ersion popup" })
			vim.keymap.set("n", "<Leader>cf", crates.show_features_popup, { desc = "(C)rates: show (f)eatures popup" })
			vim.keymap.set(
				"n",
				"<Leader>cd",
				crates.show_dependencies_popup,
				{ desc = "(C)rates: show (d)ependencies popup" }
			)

			vim.keymap.set("n", "<Leader>cu", crates.update_crate, { desc = "(C)rates: (u)pdate crate" })
			vim.keymap.set("n", "<Leader>ca", crates.update_all_crates, { desc = "(C)rates: update (a)ll crates" })
			vim.keymap.set("n", "<Leader>cU", crates.upgrade_crate, { desc = "(C)rates: (u)pgrade crate" })
			vim.keymap.set("n", "<Leader>cA", crates.upgrade_all_crates, { desc = "(C)rates: upgrade (a)ll crates" })

			vim.keymap.set("n", "<Leader>cH", crates.open_homepage, { desc = "(C)rates: open (h)omepage" })
			vim.keymap.set("n", "<Leader>cR", crates.open_repository, { desc = "(C)rates: open (r)epository" })
			vim.keymap.set("n", "<Leader>cD", crates.open_documentation, { desc = "(C)rates: open (d)ocumentation" })
			vim.keymap.set("n", "<Leader>cC", crates.open_crates_io, { desc = "(C)rates: open (c)rates.io" })
		end,
	},
	{ -- vuki656/package-info.nvim
		"vuki656/package-info.nvim",
		dependencies = { "MunifTanjim/nui.nvim" },
		event = "Bufenter package.json",
		keys = {
			{
				"<Leader>ns",
				function()
					require("package-info").show()
				end,
				silent = true,
				noremap = true,
				desc = "Show dependency versions",
			},
			{
				"<Leader>nc",
				function()
					require("package-info").hide()
				end,
				silent = true,
				noremap = true,
				desc = "Hide dependency versions",
			},
			{
				"<Leader>nt",
				function()
					require("package-info").toggle()
				end,
				silent = true,
				noremap = true,
				desc = "Toggle dependency versions",
			},
			{
				"<Leader>nu",
				function()
					require("package-info").update()
				end,
				silent = true,
				noremap = true,
				desc = "Update dependency on the line",
			},
			{
				"<Leader>nd",
				function()
					require("package-info").delete()
				end,
				silent = true,
				noremap = true,
				desc = "Delete dependency on the line",
			},
			{
				"<Leader>ni",
				function()
					require("package-info").install()
				end,
				silent = true,
				noremap = true,
				desc = "Install a new dependency",
			},
			{
				"<Leader>np",
				function()
					require("package-info").change_version()
				end,
				silent = true,
				noremap = true,
				desc = "Install a different dependency version",
			},
		},
		config = true,
	},
	{ -- norcalli/nvim-colorizer.lua
		"norcalli/nvim-colorizer.lua",
		lazy = false,
		config = function()
			require("colorizer").setup({ "*", css = { names = true } }, { names = false })
		end,
	},
	{ -- akinsho/toggleterm.nvim
		"akinsho/toggleterm.nvim",
		event = "VeryLazy",
		opts = {
			open_mapping = "<C-s>",
			direction = "float",
			float_opts = {
				border = "curved",
			},
		},
		config = true,
	},
	{ -- numToStr/Comment.nvim -- disabled
		"numToStr/Comment.nvim",
		-- enabled = false,
		event = "VeryLazy",
		config = true,
	},
	{ -- nvim-tree/nvim-web-devicons
		"nvim-tree/nvim-web-devicons",
		config = function()
			require("nvim-web-devicons").setup({
				override = {
					lir_folder_icon = {
						icon = "",
						color = "#7aa2f7",
						name = "LirFolderNode",
					},
				},
			})
		end,
	},
	{ -- windwp/nvim-autopairs
		"windwp/nvim-autopairs",
		-- enabled = false,
		event = "VeryLazy",
		opts = {
			check_ts = true,
		},
		config = true,
	},
	{ -- windwp/nvim-ts-autotag
		"windwp/nvim-ts-autotag",
		ft = { "html", "javascriptreact", "typescriptreact", "svelte" },
		config = function()
			require("nvim-ts-autotag").setup({})
		end,
	},
	{ -- smjonas/inc-rename.nvim
		"smjonas/inc-rename.nvim",
		config = true,
	},
	{ -- folke/todo-comments.nvim
		"folke/todo-comments.nvim",
		dependencies = { "nvim-lua/plenary.nvim" },
		event = "VeryLazy",
		opts = {
			keywords = {
				DEBUG = { icon = "d", color = "info" },
			},
		},
		config = true,
	},
	{ -- folke/trouble.nvim
		"folke/trouble.nvim",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		event = "VeryLazy",
		config = function()
			require("trouble").setup()

			vim.keymap.set("n", "<Leader>tt", require("trouble").toggle, { desc = "(T)oggle (t)rouble" })
			vim.keymap.set("n", "<Leader>qf", "<cmd>TroubleToggle quickfix<cr>", { desc = "Toggle (q)uick(f)ix list" })
			vim.keymap.set("n", "<Leader>ll", "<cmd>TroubleToggle loclist<cr>", { desc = "Toggle (l)oc(l)ist" })
		end,
	},
	{ -- cameron-wags/rainbow_csv.nvim
		"cameron-wags/rainbow_csv.nvim",
		ft = {
			"csv",
			"tsv",
			"csv_semicolon",
			"csv_whitespace",
			"csv_pipe",
			"rfc_csv",
			"rfc_semicolon",
		},
	},
	{ -- folke/twilight.nvim
		"folke/twilight.nvim",
		config = true,
	},
	{ -- folke/zen-mode.nvim
		"folke/zen-mode.nvim",
		dependencies = { "folke/twilight.nvim" },
		event = "VeryLazy",
		opts = {
			plugins = {
				gitsigns = { enabled = false },
			},
		},
		config = true,
	},
	{ -- lewis6991/gitsigns.nvim
		"lewis6991/gitsigns.nvim",
		version = "v0.6",
		opts = {
			on_attach = function(bufnr)
				local gs = package.loaded.gitsigns

				local function map(mode, l, r, opts)
					opts = opts or {}
					opts.buffer = bufnr
					vim.keymap.set(mode, l, r, opts)
				end

				map("n", "]c", function()
					if vim.wo.diff then
						return "]c"
					end
					vim.schedule(function()
						gs.next_hunk()
					end)
					return "<Ignore>"
				end, { expr = true, desc = { "Next git hunk" } })
				map("n", "[c", function()
					if vim.wo.diff then
						return "[c"
					end
					vim.schedule(function()
						gs.prev_hunk()
					end)
					return "<Ignore>"
				end, { expr = true, desc = "Previous git hunk" })
			end,
		},
		event = "VeryLazy",
	},
	{ -- lmburns/lf.nvim -- disabled
		"lmburns/lf.nvim",
		-- enabled = false,
		dependencies = { "plenary.nvim", "toggleterm.nvim" },
		lazy = false,
		opts = {
			border = "rounded",
			winblend = 0,
			highlights = {
				NormalFloat = { guibg = "NONE" },
			},
		},
		init = function()
			vim.g.lf_netrw = 1
		end,
		config = function(_, opts)
			if vim.g.neovide then
				opts.height = 29
				opts.width = 116
			end

			require("lf").setup(opts)
			vim.keymap.set("n", "<Leader>e", function()
				require("lf").start()
			end, { desc = "Open lf" })
		end,
	},
	{ -- tamago324/lir.nvim
		"tamago324/lir.nvim",
		lazy = false,
		enabled = false,
		dependencies = { "nvim-lua/plenary.nvim", "nvim-tree/nvim-web-devicons" },
		opts = {
			show_hidden_files = true,
			devicons = { enable = true, highlight_dirname = true },
			float = {
				win_opts = function()
					return {
						-- number = true,
						-- relativenumber = true,
						border = {
							"╭",
							"─",
							"╮",
							"│",
							"╯",
							"─",
							"╰",
							"│",
						},
						-- style = "",
					}
				end,

				curdir_window = {
					enable = true,
					highlight_dirname = true,
				},
			},
		},
		config = function(_, opts)
			local actions = require("lir.actions")
			local clipboard_actions = require("lir.clipboard.actions")
			local mark_actions = require("lir.mark.actions")

			local mappings = {
				["l"] = actions.edit,
				["<C-h>"] = actions.split,
				["<C-v>"] = actions.vsplit,
				["<C-t>"] = actions.tabedit,

				["h"] = actions.up,
				["q"] = actions.quit,

				["K"] = actions.mkdir,
				["R"] = actions.rename,
				["N"] = actions.newfile,
				["T"] = actions.touch,

				["<C-r>"] = actions.reload,

				["H"] = actions.toggle_show_hidden,

				[" "] = function()
					mark_actions.toggle_mark()
					vim.cmd.normal("j")
				end,

				["D"] = actions.delete,
				["C"] = clipboard_actions.copy,
				["X"] = clipboard_actions.cut,
				["P"] = clipboard_actions.paste,
			}
			opts.mappings = mappings

			require("lir").setup(opts)

			vim.keymap.set("n", "<Leader>e", function()
				vim.cmd.edit(".")
			end, { desc = "Open file manager" })
			vim.keymap.set("n", "<Leader>fe", require("lir.float").toggle, { desc = "Open lir in a floating window" })
			vim.api.nvim_set_hl(0, "LirDir", { link = "Directory" })
			-- DevIconLirFolderNode
		end,
	},
	{ -- lukas-reineke/indent-blankline.nvim
		"lukas-reineke/indent-blankline.nvim",
		-- enabled = false,
		main = "ibl",
		event = "VeryLazy",
		config = true,
	},
	{ -- kevinhwang91/nvim-ufo
		"kevinhwang91/nvim-ufo",
		dependencies = { "kevinhwang91/promise-async" },
		ft = { "lua", "rust", "dart", "javascript", "javascriptreact", "typescript", "typescriptreact", "python" },
		opts = {
			-- disable folding by ufo
			provider_selector = function(_, _, _)
				return { "" }
			end,
		},
		config = function(_, opts)
			vim.o.foldmethod = "expr"
			vim.opt.foldexpr = "v:lua.vim.treesitter.foldexpr()"

			require("ufo").setup(opts)
		end,
	},
	{ -- folke/which-key.nvim
		"folke/which-key.nvim",
		event = "VeryLazy",
		init = function()
			vim.o.timeout = true
			vim.o.timeoutlen = 300
		end,
		opts = {
			icons = {
				separator = "",
			},
		},
		config = true,
	},
	{ -- iamcco/markdown-preview.nvim
		"iamcco/markdown-preview.nvim",
		build = function()
			vim.fn["mkdp#util#install"]()
		end,
		ft = "markdown",
	},
	{ -- SmiteshP/nvim-navbuddy
		"SmiteshP/nvim-navbuddy",
		dependencies = {
			"SmiteshP/nvim-navic",
			"MunifTanjim/nui.nvim",
			"neovim/nvim-lspconfig",
		},
		opts = { lsp = { auto_attach = true } },
		ft = { "rust", "lua", "python", "javascript", "javascriptreact" },
	},
	{ -- folke/flash.nvim
		"folke/flash.nvim",
		event = "VeryLazy",
		version = "*",
		opts = {
			modes = {
				search = { enabled = false },
				char = { enabled = false },
			},
		},
		keys = {
			{
				"<Leader>ss",
				mode = { "n", "x", "o" },
				function()
					require("flash").jump()
				end,
				desc = "Flash",
			},
			{
				"S",
				mode = { "n", "o", "x" },
				function()
					require("flash").treesitter()
				end,
				desc = "Flash Treesitter",
			},
			{
				"r",
				mode = "o",
				function()
					require("flash").remote()
				end,
				desc = "Remote Flash",
			},
			{
				"R",
				mode = { "o", "x" },
				function()
					require("flash").treesitter_search()
				end,
				desc = "Treesitter Search",
			},
			{
				"<c-j>",
				mode = { "c" },
				function()
					require("flash").toggle()
				end,
				desc = "Toggle Flash Search",
			},
		},
	},
	{ -- elkowar/yuck.vim
		"elkowar/yuck.vim",
		ft = "yuck",
	},
	{ -- LhKipp/nvim-nu
		"LhKipp/nvim-nu",
		build = ":TSInstall nu",
		ft = "nu",
		opts = {
			use_lsp_features = false,
		},
	},
	{ -- echasnovski/mini.nvim
		"echasnovski/mini.nvim",
		version = "*",
		event = "VeryLazy",
		config = function()
			require("mini.bracketed").setup({})
			-- require("mini.comment").setup({})
			-- require("mini.files").setup({ windows = { preview = true, width_preview = 40 } })
			require("mini.surround").setup({})
			-- require("mini.trailspace").setup({})

			-- vim.keymap.set("n", "<Leader>e", require("mini.files").open, { desc = "Open mini.files" })
		end,
	},
	{ -- ThePrimeagen/harpoon
		"ThePrimeagen/harpoon",
		branch = "harpoon2",
		requires = "nvim-lua/plenary.nvim",
		event = "VeryLazy",
		keys = {
			{
				"<Leader>ha",
				function()
					require("harpoon"):list():append()
				end,
				desc = "Append to harpoon",
			},
			{
				"<Leader>hq",
				function()
					local harpoon = require("harpoon")
					harpoon.ui:toggle_quick_menu(harpoon:list())
				end,
				desc = "Harpoon quick menu",
			},
			{
				"<C-p>",
				function()
					require("harpoon"):list():prev()
				end,
			},
			{
				"<C-n>",
				function()
					require("harpoon"):list():next()
				end,
			},
			{
				"<Leader>h1",
				function()
					require("harpoon"):list():select(1)
				end,
			},
			{
				"<Leader>h2",
				function()
					require("harpoon"):list():select(2)
				end,
			},
			{
				"<Leader>h3",
				function()
					require("harpoon"):list():select(3)
				end,
			},
			{
				"<Leader>h4",
				function()
					require("harpoon"):list():select(4)
				end,
			},
		},
		config = function()
			require("harpoon"):setup()
		end,
	},
	{ -- nanotee/zoxide.vim
		"nanotee/zoxide.vim",
		lazy = false,
	},
}
