local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	-- bootstrap lazy.nvim
	vim.fn.system({ "git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", "--branch=stable",
		lazypath })
end
vim.opt.rtp:prepend(vim.env.LAZY or lazypath)

require("lazy").setup("plugins", {
	-- options
	defaults = {
		lazy = true,
	},
	install = {
		colorscheme = { "rose-pine" },
	},
	ui = {
		icons = {
			list = {
				"",
				"",
				"",
				"",
			},
		},
	},
	performance = {
		rtp = {
			reset = false,
		},
	},
})

vim.keymap.set("n", "<Leader>lz", "<cmd>Lazy<cr>", { desc = "Show (l)a(z)y" })
