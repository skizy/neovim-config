-- NAVIGATION
vim.keymap.set("n", "<A-a>", "<C-6>", { desc = "Previous buffer" })
-- vim.keymap.set("n", "<A-l>", vim.cmd.bn, { desc = "Next buffer" })
vim.keymap.set("i", "<A-a>", "<C-6>", { desc = "Previous buffer" })
-- vim.keymap.set("i", "<A-l>", vim.cmd.bn, { desc = "Next buffer" })
vim.keymap.set("t", "<A-n>", "<C-\\><C-N>", { desc = "Switch to normal mode from terminal" })

-- OTHER
vim.keymap.set("n", "<Leader>w", vim.cmd.w, { desc = "(W)rite buffer to file" })
vim.keymap.set("n", "<Leader>x", vim.cmd.bd, { desc = "Close buffer" })
vim.keymap.set("n", "<C-k>", function() vim.o.hlsearch = not vim.o.hlsearch end, { desc = "Switch search highlighting" })

vim.keymap.set("i", "<C-c>", "<Esc>", { noremap = true, desc = "Change <C-c> to <Esc> because it breaks some plugins" })

-- vim.keymap.set("n", "<Leader>e", vim.cmd.Explore, { desc = "Open netrw" })
