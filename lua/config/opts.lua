vim.o.termguicolors = true

vim.o.number = true
vim.o.relativenumber = true

vim.o.autoindent = true
vim.o.smartindent = true
vim.o.tabstop = 4
vim.o.shiftwidth = 4
vim.o.smarttab = true
vim.o.softtabstop = 4

vim.o.mouse = ""

vim.o.wrap = false

vim.opt.completeopt = { "menu", "menuone", "noselect" }

vim.o.hls = false

vim.o.swapfile = false

vim.o.splitright = true
vim.o.splitbelow = true

-- vim.opt.foldcolumn = "1"
-- vim.opt.foldlevel = 99
-- vim.opt.foldlevelstart = 99
vim.opt.foldmethod = "marker"
vim.opt.foldenable = true
vim.opt.foldopen = {}

vim.opt.showmode = false
vim.opt.signcolumn = "auto:2"

vim.opt.cursorline = true

-- vim.opt.list = true
-- vim.opt.listchars = "tab:│ ,trail:-,nbsp:+"

vim.g.mapleader = " "

vim.g.netrw_bufsettings = "noma nomod nu rnu nowrap ro nobl"
