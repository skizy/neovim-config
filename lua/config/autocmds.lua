vim.api.nvim_create_autocmd("BufWinLeave", {
	pattern = { "?*\\.rs", "?*\\.lua", "?*\\.py", "?*\\.js", "?*\\.jsx", "?*\\.ts", "?*\\.tsx" },
	callback = function()
		if not vim.o.diff then
			vim.cmd.mkview()
		end
	end,
})

vim.api.nvim_create_autocmd("BufWinEnter", {
	pattern = { "?*\\.rs", "?*\\.lua", "?*\\.py", "?*\\.js", "?*\\.jsx", "?*\\.ts", "?*\\.tsx" },
	callback = function()
		if not vim.o.diff then
			vim.cmd.loadview({ mods = { silent = true, emsg_silent = true } })
		end
	end,
})

vim.api.nvim_create_autocmd("TextYankPost", {
	desc = "Highlight when yanking text",
	group = vim.api.nvim_create_augroup("highlight-yank", { clear = true }),
	callback = function()
		vim.highlight.on_yank()
	end,
})
