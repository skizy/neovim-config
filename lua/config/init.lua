require("config.opts")

require("config.lazy")

require("config.keymap")

require("config.autocmds")

-- require("config.connect_brackets")

vim.cmd.colorscheme("rose-pine")

vim.filetype.add({
	pattern = { [".*/hyprland%.conf"] = "hyprlang" },
})

if vim.g.neovide then
	-- vim.opt.linespace = -2

	vim.opt.guifont = "Iosevka Custom Extended:h15"

	vim.g.neovide_transparency = 0.8

	vim.g.neovide_hide_mouse_when_typing = true

	vim.g.neovide_fullscreen = false

	vim.g.neovide_padding_top = 10
	vim.g.neovide_padding_bottom = 10
	vim.g.neovide_padding_right = 10
	vim.g.neovide_padding_left = 10
end
