local M = {}

local function escape(str)
	return vim.api.nvim_replace_termcodes(str, true, false, true)
end

M.connect_brackets = function()
	local autopairs = require("nvim-autopairs")

	local has_autopairs
	if autopairs == nil then
		has_autopairs = false
	else
		has_autopairs = true
	end

	local cursor = vim.api.nvim_win_get_cursor(0)
	local row = cursor[1]

	if row < 2 or row == vim.api.nvim_buf_line_count(0) then
		if has_autopairs then
			---@diagnostic disable-next-line: need-check-nil
			return autopairs.autopairs_bs()
		else
			return escape("<bs>")
		end
	end

	local lines_start = row - 2
	local lines_end = row + 1

	local lines = vim.api.nvim_buf_get_lines(0, lines_start, lines_end, true)

	local bottom_match
	if lines[3]:match("^%s*}$") then bottom_match = true else bottom_match = false end

	local empty_match = lines[2] == ''

	local top_match
	if lines[1]:match("{$") then top_match = true else top_match = false end

	-- local replacement_array = { lines[1] .. "}" } -- if changing to replace
	if bottom_match and empty_match and top_match then
		if row == vim.api.nvim_buf_line_count(0) - 1 then
			return escape("<esc>2dd$a}<left>")
		else
			return escape("<esc>2ddk$a}<left>")
		end
	else
		if has_autopairs then
			---@diagnostic disable-next-line: need-check-nil
			return autopairs.autopairs_bs()
		else
			return escape("<bs>")
		end
	end
end

vim.keymap.set(
	"i",
	"<bs>",
	M.connect_brackets,
	{ expr = true, noremap = true, replace_keycodes = false, desc = "Connect brackets from different lines" }
)

return M
